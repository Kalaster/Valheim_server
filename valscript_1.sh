#!/bin/bash

echo installing Valheim server...
sleep 1
cp sources.list /etc/apt/sources.list
echo Updating Distro...
apt update && apt upgrade -y
echo Updated. Now adding the Multiverse repository
sleep 1

apt install software-properties-common
dpkg --add-architecture i386
apt update
apt install lib32gcc-s1 steamcmd net-tools neofetch -y
sleep 1

echo Done. Creating steam user...

sleep 1
useradd -m -s /bin/bash steam
echo Done.
echo Now for the users password...
passwd steam
echo Adding steam user to sudoers group...
sleep 1

usermod -aG steam
echo Done. Making Syslink to steamcmd
sleep 1

ln -s /usr/games/steamcmd /home/steam/steamcmd
echo Done. Running Steamcmd and installing Valheim server...
sleep 1

steamcmd +force_install_dir /home/steam/valheimserver +login anonymous +app_update 896660 validate +exit
echo Done. Adding start_valheim.sh file
sleep 1

cp start_valheim.sh /home/steam/valheimserver/
echo Done. Adding Valheim service...
sleep 1

cp valheim.service /etc/systemd/system/valheim.service
echo Done. Adding Bash aliases...
sleep 1

cp .bash_alias /home/steam/
echo Done.
sleep 1

cp .bashrc /home/steam/
echo Done. Reloading daemon
sleep 1

systemctl daemon-reload
echo Done. Enabling Valheim Service at startup...
sleep 1

systemctl enable valheim
echo Done. Bringing you home...
sleep 1 

cd /home/steam
passwd steam
echo Done.
su - steam
source /home/steam/.bash_alias
echo Valheim Server is ready. 
