#!/bin/bash

echo installing Valheim server...
sleep 1
echo Updating Distro...
sudo apt update && sudo apt upgrade -y
echo Updated. Now adding the Multiverse repository
sleep 1
sudo add-apt-repository multiverse
echo Done. Now adding i386 architecture
sleep 1
sudo dpkg --add-architecture i386
echo Done. Updating again...
sleep 1
sudo apt update
echo Done. Now installing steamcmd net-tools and neofetch
sleep 1
sudo apt install -y steamcmd net-tools neofetch
echo Done. Creating steam user...
sleep 1
sudo useradd -m -s /bin/bash steam
echo Done.
echo Now for the users password...
sudo passwd steam
echo Adding steam user to sudoers group...
sleep 1
sudo usermod -aG sudo steam
echo Done. Making Syslink to steamcmd
sleep 1
sudo ln -s /usr/games/steamcmd /home/steam/steamcmd
echo Done. Running Steamcmd and installing Valheim server...
sleep 1
steamcmd +force_install_dir /home/steam/valheimserver +login anonymous +app_update 896660 validate +exit
echo Done. Adding start_valheim.sh file
sleep 1
sudo cp start_valheim.sh /home/steam/valheimserver/
echo Done. Adding Valheim service...
sleep 1
sudo cp valheim.service /etc/systemd/system/valheim.service
echo Done. Adding Bash aliases...
sleep 1
sudo cp .bash_alias /home/steam/
echo Done.
sleep 1
sudo cp .bashrc /home/steam/
echo Done. Reloading daemon
sleep 1
sudo systemctl daemon-reload
echo Done. Enabling Valheim Service at startup...
sleep 1
sudo systemctl enable valheim
echo Done. Bringing you home...
sleep 1 
cd /home/steam
passwd steam
echo Done.
su - steam
source /home/steam/.bash_alias
echo Valheim Server is ready. 
